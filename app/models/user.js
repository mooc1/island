
const bcrypt = require('bcryptjs');
const { sequelize } = require('../../core/db');
const { Sequelize, Model } = require('sequelize');

class User extends Model{

    static async verifyEmailPassword(email, plainPassword){
        const user = await User.findOne({
            where:{
                email
            }
        });
        if(!user){
            throw new global.errs.NotFound('用户不存在')
        }
        const correct = bcrypt.compareSync(plainPassword, user.password);
        if(!correct){
            throw new global.errs.AuthFailed
        }
        return user;
    }

    static async getUserByOpenid(openid){
        const user = await User.findOne({
            where:{
                openid
            }
        })
        return user
    }

    static async registerByOpenid(openid) {
        return await User.create({
            openid
        })
    }
}

// 初始化数据库中表格字段
User.init({
    id:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true,
    },
    nickname:Sequelize.STRING(128),
    email:{
        type:Sequelize.STRING,
        unique:true,
    },
    password:{
        type:Sequelize.STRING,
        set(val){   // modle 的属性操作
            const salt = bcrypt.genSaltSync(10);
            const psw = bcrypt.hashSync(val, salt);
            this.setDataValue('password', psw);
        }
    },
    openid:{
        type:Sequelize.STRING(64),
        unique:true
    },
    test:Sequelize.STRING,
    test1:Sequelize.STRING,
}, {
    sequelize,
    tableName:'user'
})


module.exports = { User }


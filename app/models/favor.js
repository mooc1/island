
const {sequelize} = require('../../core/db');
const {Sequelize,Model} = require('sequelize');
const { Art } = require('./art')

class Favor extends Model{
    // 业务表
    static async like(art_id, type, uid){
        // 1: favor 添加记录 2：修改classics 总点赞的数目
        // 1 和 2 需要同时做
        // 数据库事务： 对操作要么都成功要么就都不做以此来保持数据的一致性
        const favor = await Favor.findOne({
            where:{
                art_id, 
                type, 
                uid
            }
        })

        if(favor){
            throw new global.errs.LikeError();
        }

        // 这里的transaction 一定需要return
        return sequelize.transaction(async t=>{
            await Favor.create({
                art_id, 
                type, 
                uid
            }, {transaction:t})
            const art = await Art.getData(art_id, type);
            await art.increment('fav_nums',{by:1, transaction:t});
        })

    }

    static async disLike(art_id, type, uid) {
        const favor = await Favor.findOne({
            where: {
                art_id,
                type,
                uid
            }
        })
        if (!favor) {
            throw new global.errs.DislikeError()
        }
        // Favor 表 favor 记录
        return sequelize.transaction(async t => {
            await favor.destroy({
                force: true,
                transaction: t
            })
            const art = await Art.getData(art_id, type, false)
            await art.decrement('fav_nums', {
                by: 1,
                transaction: t
            })
        })
    }


    static async userLikeIt(art_id, type, uid) {
        const favor = await Favor.findOne({
            where: {
                uid,
                art_id,
                type,
            }
        })
        return favor ? true : false
    }

}

Favor.init({
    uid: Sequelize.INTEGER,
    art_id: Sequelize.INTEGER,
    type: Sequelize.INTEGER
}, {
    sequelize,
    tableName: 'favor'
})


module.exports = {
    Favor
}

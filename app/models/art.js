
const {
    Movie,
    Sentence,
    Music
} = require('./classic')

class Art{

    constructor(art_id, type) {
        this.art_id = art_id
        this.type = type
    }
    
    // sequlize 的一个bug,使用scope查询之后，再对这个实例操作就会报一个sql的bug.
    static async getData(artId, type, useScope = false){
        let art = null;
        const finder = {
           where:{
            id:artId,
           }
        };
        const scope = useScope ? 'bh' : null;
        switch(type){
            case 100:
                // 使用全局注册的scope 查询 注册的名字为bh，这样去除三个时间字段 
                art = await Movie.scope(scope).findOne(finder);
                break;
            case 200:
                art = await Music.scope(scope).findOne(finder);
                break;
            case 300:
                art = await Sentence.scope(scope).findOne(finder);
                break;
            case 400:
                    break;  
            default:
                break;              
        }
        // 这里返回了art的实例，使用scope 查询之后在对这个art 实例进行任何操作就会报sql操作
        return art;
    }

    async getDetail(uid) {
        const {
            Favor
        } = require('./favor')
        const art = await Art.getData(this.art_id, this.type)
        if (!art) {
            throw new global.errs.NotFound()
        }

        const like = await Favor.userLikeIt(
            this.art_id, this.type, uid)
        // art.setDataValue('like_status',like)
        return {
            art,
            like_status: like
        }
    }

}

module.exports = {
    Art,
}
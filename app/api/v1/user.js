
const Router = require('koa-router');

const { RegisterValidator } = require('./../../validator/validator');
const { success } = require('./../../lib/helper');

const { User } = require('./../../models/user'); 

const router = new Router({
    prefix:'/v1/user'           // 该router 的共同前缀
});

// 注册 
router.post('/register', async(ctx)=>{
    // email pwd1 pwd2 nickname
    const v = await new RegisterValidator().validate(ctx);
    const user = {
        email:v.get('body.email'),
        password:v.get('body.password2'),
        nickname:v.get('body.nickname')
    };
    await User.create(user);
    success();
});

module.exports = router;
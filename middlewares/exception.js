
const  { HttpException } = require('./../core/httpException');

// 全局异常处理
const catchError = async (ctx, next)=>{
    try {
        await next()
    } catch (error) {
        const isHttpException = error instanceof HttpException;
        const isDev = global.config.environment === 'dev';
        if( isDev && !isHttpException){
            //throw error
        }
        
        if(isHttpException){    // 已知错误.
            ctx.body = {
                msg:error.msg,
                error_code:error.errorCode,
                request:`${ctx.method} ${ctx.path}`,
            }
            ctx.status = error.code;
        }else{  // 系统未知错误
            ctx.body = {
                msg:error.message,
                error_code:999,
                request:`${ctx.method} ${ctx.path}`,
            }
            ctx.status = 500;
        }
    }
}

module.exports = catchError;
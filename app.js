
// 给路径配置别名
require('module-alias/register')

const Koa = require('koa');
const parser = require('koa-bodyparser');
const InitManager = require('./core/init');
const catchError = require('./middlewares/exception');

// 应用程序对象，上面包含许多中间件
const app = new Koa();

app.use(catchError);
app.use(parser()); 

InitManager.initCore(app);

app.listen(3000);
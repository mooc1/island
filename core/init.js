const requireDirectory = require('require-directory');
const Router = require('koa-router');


class InitManager{

    static initCore(app){
        // 入口方法
        InitManager.app = app;
        InitManager.initLoadRouters();
        InitManager.loadHttpException();
        InitManager.loadConfig();
    }

    static initLoadRouters(){
        const apiDirectory = `${process.cwd()}/app/api`
        const modules = requireDirectory(module,apiDirectory,{
            visit:whenLoadModule
        });
        
        function whenLoadModule(obj){
            if(obj instanceof Router){
                InitManager.app.use(obj.routes())
            }
        }
    }

    static loadHttpException(){
        const errors = require('./httpException');
        global.errs = errors;
    }

    static loadConfig(path = ''){
        const configPath = path || process.cwd() + '/config/config.js'
        const config = require(configPath)
        global.config = config
    }
}

module.exports = InitManager;

// 利用 sequelize 将代码中的模型转化为数据库中的表，这样就不用手动在数据库中一个个建表了
const Sequelize = require('sequelize');
const  {
    dbName,
    host,
    port,
    user,
    password
} = require('./../config/config').database;

const sequelize = new Sequelize(dbName, user, password, {
    dialect:'mysql',
    host, 
    port,
    logging:false,   // 在命令行中显示原始的sql语句.
    timezone: '+08:00', // 以北京时间记录相关数据
    define:{
        timestamps:true,
        paranoid:true,  // 增加 deleteAt
        createdAt:'created_at', // 修改名称，将数据库中 createdAt 改为 created_at
        updatedAt:'updated_at', // 不修改返回给前端的是 updatedAt
        deletedAt:'deleted_at',
        underscored:true,  // 将驼峰改为下划线命名方式
        scopes:{    // 定义全局的scope 这样可以在每个模型上去除下面这三个字段.
            bh:{
                attributes:{
                    exclude:['updated_at','deleted_at','created_at']
                }
            }
        }
    }
});

sequelize.sync({
    force:false
});

module.exports = {
    sequelize
};
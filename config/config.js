module.exports = {
    // dev, prod    
    environment:'dev',
    database:{
        dbName:'mooc',
        host:'localhost',   // 数据库的ip地址 本机即为 localhost
        port:3306,
        user:'root',
        password:'happy@1012'
    },
    security:{
        secretKey:"abcdefg",
        expiresIn:60*60*24*30
    },
    wx:{
        appId:'wxe6a7c2141cf079d7',
        appSecret:'4e4abd66130df76f7bb5f100681a1a57',
        loginUrl:'https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code'
    },
}

